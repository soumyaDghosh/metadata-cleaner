# SPDX-FileCopyrightText: Metadata Cleaner contributors
# SPDX-License-Identifier: GPL-3.0-or-later

variables:
  APP_ID: fr.romainvigier.MetadataCleaner
  MANIFEST: build-aux/fr.romainvigier.MetadataCleaner.yaml
  APP_POT: application/po/${APP_ID}.pot
  HELP_POT: help/C/${APP_ID}.pot
  PACKAGE_REGISTRY_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/metadata-cleaner

workflow:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

stages:
  - prepare
  - check
  - build
  - bundle
  - deploy

.python:
  image: python:latest
  variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  cache:
    key: python
    paths:
      - .cache/pip
      - venv/
  before_script:
    - python -V
    - pip install virtualenv
    - virtualenv venv
    - source venv/bin/activate

screenshots:
  stage: prepare
  image:
    name: registry.gitlab.com/rmnvgr/uishooter:latest
    entrypoint: [""]
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
      - application/po/*.po
      - help/LINGUAS
      - screenshots/**/*
  before_script:
    - pip install langcodes
  script:
    - python3 screenshots/screenshot.py
  artifacts:
    paths:
      - help/
      - resources/
      - website/
    expire_in: 1 day

translations:
  image: freedesktopsdk/sdk:22.08
  stage: prepare
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
        - application/**/*.py
        - application/data/**/*.desktop
        - application/data/**/*.ui
        - application/data/**/*.gschema.xml
        - application/data/**/*.metainfo.xml
        - application/po/POTFILES
        - help/C/**/*.page
  script:
    - meson builddir
    - meson compile -C builddir ${APP_ID}-pot
    - meson compile -C builddir help-${APP_ID}-pot
  artifacts:
    paths:
      - $APP_POT
      - $HELP_POT
    expire_in: 1 day

manifest:
  extends: .python
  stage: prepare
  interruptible: true
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - pip install PyYAML
    - python build-aux/prepare_manifest.py
  artifacts:
    paths:
      - $MANIFEST
    expire_in: 1 day

release-notes:
  extends: .python
  stage: prepare
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - ./build-aux/get_release_notes.py $CI_COMMIT_TAG > release-notes.md
  artifacts:
    paths:
      - release-notes.md
    expire_in: 1 day

mypy:
  extends: .python
  stage: check
  interruptible: true
  rules:
    - changes:
      - application/**/*.py
  script:
    - pip install mypy
    - mypy --ignore-missing-imports ./application

pydocstyle:
  extends: .python
  stage: check
  interruptible: true
  rules:
    - changes:
      - application/**/*.py
  script:
    - pip install pydocstyle
    - pydocstyle ./application

pycodestyle:
  extends: .python
  stage: check
  interruptible: true
  rules:
    - changes:
      - application/**/*.py
  script:
    - pip install pycodestyle
    - pycodestyle ./application

reuse:
  image:
    name: fsfe/reuse:latest
    entrypoint: [""]
  stage: check
  interruptible: true
  needs:
    - job: screenshots
      optional: true
      artifacts: true
    - job: translations
      optional: true
      artifacts: true
  script:
    - reuse lint

flatpak-repo:
  stage: build
  interruptible: true
  needs:
    - job: screenshots
      optional: true
      artifacts: true
    - job: translations
      optional: true
      artifacts: true
    - job: manifest
      optional: true
      artifacts: true
    - job: mypy
      optional: true
    - job: pydocstyle
      optional: true
    - job: pycodestyle
      optional: true
    - job: reuse
      optional: true
  image: fedora:latest
  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: manual
    - if: $CI_COMMIT_TAG
  cache:
    - key: flatpak-repo
      paths:
        - .flatpak-builder
  before_script:
    - dnf install --setopt=install_weak_deps=False --nodocs -y flatpak flatpak-builder git
  script:
    - flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    - flatpak-builder --install-deps-from=flathub --repo=repo --default-branch=$CI_COMMIT_REF_SLUG --ccache --disable-rofiles-fuse builddir $MANIFEST
  artifacts:
    name: $CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG-flatpak-repo
    paths:
      - repo
    expire_in: 1 day

app:
  stage: bundle
  interruptible: true
  needs:
    - job: flatpak-repo
      artifacts: true
  image: freedesktopsdk/flatpak:22.08
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
  script:
    - flatpak build-bundle ./repo $APP_ID.flatpak $APP_ID $CI_COMMIT_REF_SLUG
  artifacts:
    name: $CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG-flatpak
    expose_as: Flatpak bundle
    paths:
      - fr.romainvigier.MetadataCleaner.flatpak
    expire_in: 1 day

debug:
  stage: bundle
  interruptible: true
  needs:
    - job: flatpak-repo
      artifacts: true
  image: freedesktopsdk/flatpak:22.08
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
  script:
    - flatpak build-bundle --runtime ./repo $APP_ID.Debug.flatpak $APP_ID.Debug $CI_COMMIT_REF_SLUG
  artifacts:
    name: $CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG-flatpak-debug
    expose_as: Flatpak debug symbols
    paths:
      - fr.romainvigier.MetadataCleaner.Debug.flatpak
    expire_in: 1 day

package:
  stage: deploy
  needs:
    - job: app
      artifacts: true
    - job: debug
      artifacts: true
  image: curlimages/curl:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
      curl \
      --header "JOB-TOKEN: $CI_JOB_TOKEN" \
      --upload-file ${APP_ID}.flatpak \
      "${PACKAGE_REGISTRY_URL}/$CI_COMMIT_TAG/${APP_ID}.flatpak"
    - |
      curl \
      --header "JOB-TOKEN: $CI_JOB_TOKEN" \
      --upload-file ${APP_ID}.Debug.flatpak \
      "${PACKAGE_REGISTRY_URL}/$CI_COMMIT_TAG/${APP_ID}.Debug.flatpak"

release:
  stage: deploy
  needs:
    - package
    - job: release-notes
      artifacts: true
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo "Creating release for $CI_COMMIT_TAG"
  release:
    name: Metadata Cleaner $CI_COMMIT_TAG
    tag_name: $CI_COMMIT_TAG
    description: release-notes.md
    assets:
      links:
        - name: Flatpak bundle
          url: ${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}/${APP_ID}.flatpak
          filepath: /${APP_ID}.flatpak
          link_type: package

pages:
  stage: deploy
  needs:
    - job: reuse
      optional: true
    - job: screenshots
      optional: true
      artifacts: true
    - job: translations
      optional: true
      artifacts: true
  image: debian:stable-slim
  rules:
    - if: $CI_COMMIT_TAG
  before_script:
    - apt update
    - apt install --no-install-recommends -y gzip brotli
  script:
    - cp -r website public
    - cp -r resources/screenshots public/screenshots
    - find public -type f -regex '.*\.\(|html\|css\|svg\|png\|woff\|woff2\)$' -exec gzip -f -k {} \;
    - find public -type f -regex '.*\.\(|html\|css\|svg\|png\|woff\|woff2\)$' -exec brotli -f -k {} \;
  artifacts:
    paths:
      - public
    expire_in: 1 day

push:
  stage: deploy
  needs:
    - job: reuse
      optional: true
    - job: screenshots
      artifacts: true
      optional: true
    - job: translations
      artifacts: true
      optional: true
  image: bitnami/git:latest
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  before_script:
    - git config --global user.email "${CI_BOT_EMAIL}"
    - git config --global user.name "${CI_BOT_USERNAME}"
    - git remote set-url origin "https://ci-bot:${CI_BOT_ACCESS_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git"
  script:
    - git status
    - |
      if [[ -n $(git status --porcelain $APP_POT) ]]; then
        git add $APP_POT
        git commit -m "Application: Update translation template"
      fi
    - |
      if [[ -n $(git status --porcelain $HELP_POT) ]]; then
        git add $HELP_POT
        git commit -m "Help: Update translation template"
      fi
    - |
      if [[ -n $(git status --porcelain help) ]]; then
        git add help
        git commit -m "Help: Update screenshots"
      fi
    - |
      if [[ -n $(git status --porcelain resources) ]]; then
        git add resources
        git commit -m "Resources: Update screenshots"
      fi
    - |
      if [[ -n $(git status --porcelain website) ]]; then
        git add website
        git commit -m "Website: Update screenshots"
      fi
  after_script:
    - git pull origin main
    - git push -o ci.skip origin HEAD:main
