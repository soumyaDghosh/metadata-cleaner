<!--
SPDX-FileCopyrightText: Metadata Cleaner contributors
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Metadata Cleaner

![](./resources/icons/icon.svg)

Metadata within a file can tell a lot about you. Cameras record data about when and where a picture was taken and which camera was used. Office applications automatically add author and company information to documents and spreadsheets. This is sensitive information and you may not want to disclose it.

This tool allows you to view metadata in your files and to get rid of it, as much as possible.

Under the hood, it relies on [mat2](https://0xacab.org/jvoisin/mat2) to parse and remove the metadata.

## Screenshots

<a href="./resources/screenshots/1.png"><img src="./resources/screenshots/1.png" alt="Welcome screen" height="250"></a>
<a href="./resources/screenshots/2.png"><img src="./resources/screenshots/2.png" alt="Files view" height="250"></a>
<a href="./resources/screenshots/3.png"><img src="./resources/screenshots/3.png" alt="Metadata details" height="250"></a>
<a href="./resources/screenshots/4.png"><img src="./resources/screenshots/4.png" alt="Cleaned files" height="250"></a>

## Installing

Metadata Cleaner is available as a Flatpak on Flathub:

<a href="https://flathub.org/apps/details/fr.romainvigier.MetadataCleaner"><img src="https://flathub.org/assets/badges/flathub-badge-en.png" alt="Download on Flathub" width="240"></a>

## Building from source

Dependencies:

- `gtk4` >= 4.6
- `libadwaita-1` >= 1.2
- `pygobject-3.0`
- `python3`
- Python 3 `libmat2` module and [its dependencies](https://0xacab.org/jvoisin/mat2#requirements)

Metadata Cleaner uses the meson build system:

```sh
meson builddir
meson install -C builddir
```

Flatpak building is also available and requires the GNOME platform and SDK runtimes:

```sh
flatpak-builder --force-clean --user --install builddir build-aux/fr.romainvigier.MetadataCleaner.yaml
```

## Contributing

You can contribute to the documentation, translation or code, see [`CONTRIBUTING.md`](./CONTRIBUTING.md).

The application is part of [GNOME Circle](https://circle.gnome.org/), so the [GNOME Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct) applies to this project.
